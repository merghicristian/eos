const express = require('express')
const router = express.Router()

/* GET gradients page. */
router.get('/', (req, res, next) => {
  if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production') {
    res.redirect('/')
  } else {
    res.render('internal/gradients')
  }
})

module.exports = router
